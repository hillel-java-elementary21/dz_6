package org.homework.list;

import lombok.Data;

import java.util.*;

public class MySet<T> implements Set<T> {

    private static final int DEFAULT_CAPACITY = 16;
    private final Node[] arr;
    private int size = 0;

    @Data
    private static class Node<T> {
        T value;
        Node<T> next;
    }

    public MySet() {
        this(DEFAULT_CAPACITY);
    }

    public MySet(int capacity) {
        this.arr = new Node[capacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        int i = calcIndex(o);
        if (arr[i] == null) return false;
        for (Node<T> cur = arr[i]; cur != null; cur = cur.next) {
            if (cur.value.equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            Node<T> cur;
            int arrIndex = 0;
            boolean flag = true;

            @Override
            public boolean hasNext() {

                for (int i = arrIndex; i < arr.length; i++) {
                    if (arr[i] == null) {
                        arrIndex++;
                        continue;
                    } else if (arr[i] != null && arr[i].next == null) {
                        arrIndex++;
                        cur = arr[i];
                        return true;
                    } else {
                        if (flag) {
                            cur = arr[i];
                            flag = false;
                            return true;
                        } else if (cur.next != null) {
                            cur = cur.next;
                            return true;
                        } else {
                            flag = true;
                            arrIndex++;
                        }
                    }
                }
                return false;
            }

            @Override
            public T next() {
                return cur.getValue();
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        a = Arrays.copyOf(a, size);
        int i = 0;
        for (T t : this) {
            a[i++] = (T1) t;
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        int i = calcIndex(t);
        if (arr[i] == null) {
            arr[i] = new Node<T>();
            arr[i].value = t;
            size++;
            return true;
        }
        for (Node<T> cur = arr[i]; cur != null; cur = cur.next) {
            if (cur.value.equals(t)) {
                return false;
            } else if (cur.next == null) {
                cur.next = new Node<T>();
                cur.next.value = t;
                size++;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        Node<T> prev = null;
        if (!contains(o)) return false;
        if (arr[calcIndex(o)].next == null) {
            arr[calcIndex(o)] = null;
            size--;
            return true;
        }
        if (arr[calcIndex(o)].next != null) {
            for (Node<T> cur = arr[calcIndex(o)]; cur != null; prev = cur, cur = cur.next) {
                if (cur.value.equals(o)) {
                    if (prev != null) prev.setNext(cur.next);
                    if (cur.next == null) cur = null;
                    if (prev == null) arr[calcIndex(o)] = cur.next;
                    size--;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean result = false;
        for (Object o : c) {
            if (contains(o)) {
                result = true;
            } else {
                result = false;
                break;
            }
        }
        return result;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        c.forEach(this::add);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean delFlag;
        boolean result = false;
        for (T t : this) {
            delFlag = false;
            for (Object o : c) {
                if (Objects.equals(t, o)) {
                    delFlag = false;
                    break;
                } else {
                    delFlag = true;
                }
            }
            if (delFlag) {
                remove(t);
                result = true;
            }
        }
        return result;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean flag = false;
        for (Object o : c) {
            if (contains(o)) {
                remove(o);
                flag = true;
            }
        }
        return flag;
    }

    @Override
    public void clear() {
        for (int i = 0; i < arr.length; i++) {
            arr[i] = null;
            size = 0;
        }
    }

    private int calcIndex(Object o) {
        return (o.hashCode() % arr.length);
    }
}
