package org.homework.list;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Iterator;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MySetTest {

    @Test
    @DisplayName("Тест итератора")
    void testIterator() {
        Set<Integer> set = new MySet<>();
        set.add(11);
        set.add(33);
        set.add(44);
        set.add(55);
        set.add(50);
        set.add(66);
        set.add(81);
        set.add(49);

        Iterator<Integer> iterator = set.iterator();
        assertTrue(iterator.hasNext());
        assertEquals(33, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(81, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(49, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(50, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(66, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(55, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(11, iterator.next());
        assertTrue(iterator.hasNext());
        assertEquals(44, iterator.next());
        assertFalse(iterator.hasNext());

        Set<Integer> set2 = new MySet<>(5);
        set2.add(11);
        set2.add(33);
        set2.add(44);
        set2.add(55);
        set2.add(50);
        set2.add(66);
        set2.add(81);
        set2.add(49);

        Iterator<Integer> iterator2 = set2.iterator();
        assertTrue(iterator2.hasNext());
        assertEquals(55, iterator2.next());
        assertTrue(iterator2.hasNext());
        assertEquals(50, iterator2.next());
        assertTrue(iterator2.hasNext());
        assertEquals(11, iterator2.next());
        assertTrue(iterator2.hasNext());
        assertEquals(66, iterator2.next());
        assertTrue(iterator2.hasNext());
        assertEquals(81, iterator2.next());
        assertTrue(iterator2.hasNext());
        assertEquals(33, iterator2.next());
        assertTrue(iterator2.hasNext());
        assertEquals(44, iterator2.next());
        assertTrue(iterator2.hasNext());
        assertEquals(49, iterator2.next());
        assertFalse(iterator2.hasNext());
    }

    @Test
    @DisplayName("Тест содержит ли элемент")
    void testContains() {
        Set<Integer> set = new MySet<>(16);

        set.add(11);
        set.add(33);
        set.add(44);
        set.add(55);
        set.add(50);
        set.add(66);
        set.add(81);
        set.add(49);

        assertTrue(set.contains(33));
        assertTrue(set.contains(81));
        assertTrue(set.contains(11));
        assertTrue(set.contains(50));
        assertFalse(set.contains(91));
        assertFalse(set.contains(30));
    }

    @Test
    @DisplayName("Тест преобразование в массив")
    public void testToArray() {
        Set<Integer> list = new MySet<>();
        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);

        Integer[] arr1 = list.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{80, 20, 10, 60}, arr1);

        Set<Integer> list2 = new MySet<>(10);
        list2.add(10);
        list2.add(20);
        list2.add(60);
        list2.add(80);

        Integer[] arr2 = list2.toArray(new Integer[0]);
        assertArrayEquals(new Object[]{10, 20, 60, 80}, arr2);
    }

    @Test
    @DisplayName("Тест удаление объекта")
    public void testRemove() {
        Set<Integer> list = new MySet<>(8);
        list.add(11);
        list.add(33);
        list.add(44);
        list.add(55);
        list.add(50);
        list.add(16);
        list.add(81);
        list.add(49);
        list.add(15);

        assertFalse(list.remove(150));
        assertTrue(list.remove(33));

        Integer[] arr1 = list.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{16, 81, 49, 50, 11, 44, 55, 15}, arr1);

        assertTrue(list.remove(49));

        Integer[] arr2 = list.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{16, 81, 50, 11, 44, 55, 15}, arr2);

        assertFalse(list.remove(49));
        assertTrue(list.remove(16));

        Integer[] arr3 = list.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{81, 50, 11, 44, 55, 15}, arr3);
    }

    @Test
    @DisplayName("Тест проверки содержет ли коллекцию")
    public void testContainsAll() {
        Set<String> list = new MySet<>();
        Set<String> list2 = new MySet<>();
        Set<String> list3 = new MySet<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        list.add("abc");

        list2.add("a");
        list2.add("d");
        list2.add("b");

        list3.add("a");
        list3.add("e");
        list3.add("c");

        assertTrue(list.containsAll(list2));
        assertFalse(list.containsAll(list3));
        assertTrue(list.containsAll(list));
        assertFalse(list2.containsAll(list));
    }

    @Test
    @DisplayName("Тест добавление коллекции")
    public void testAddAll() {
        Set<Integer> list = new MySet<>(10);
        Set<Integer> list2 = new MySet<>();
        list.add(10);
        list.add(20);
        list.add(30);
        list.add(40);
        list.add(50);

        list2.add(1);
        list2.add(2);
        list2.add(3);
        list2.add(4);
        list2.add(5);

        list.addAll(list2);

        Integer[] arr1 = list.toArray(new Integer[0]);
        assertArrayEquals(new Integer[]{10, 20, 30, 40, 50, 1, 2, 3, 4, 5}, arr1);

    }

    @Test
    @DisplayName("Тест удаление коллекции")
    public void testRemoveAll() {
        Set<Integer> list = new MySet<>();
        Set<Integer> list2 = new MySet<>();
        Set<Integer> list3 = new MySet<>();

        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);

        list2.add(10);
        list2.add(60);
        list2.add(80);

        list3.add(15);

        assertTrue(list.removeAll(list2));
        assertArrayEquals(new Integer[]{20, 100}, list.toArray(new Integer[0]));

        assertFalse(list.removeAll(list3));
        assertArrayEquals(new Integer[]{20, 100}, list.toArray(new Integer[0]));
    }

    @Test
    @DisplayName("Тест удаление вне коллекции")
    public void testRetainAll() {
        Set<Integer> list = new MySet<>();
        Set<Integer> list2 = new MySet<>();
        Set<Integer> list3 = new MySet<>();

        list.add(10);
        list.add(20);
        list.add(60);
        list.add(80);
        list.add(100);

        list2.add(10);
        list2.add(60);
        list2.add(80);

        list3.add(15);
        list3.add(1);
        list3.add(80);

        assertTrue(list.retainAll(list2));
        assertArrayEquals(new Integer[]{80, 10, 60}, list.toArray(new Integer[0]));

        assertFalse(list.retainAll(list2));

        assertTrue(list.retainAll(list3));
        assertArrayEquals(new Integer[]{80}, list.toArray(new Integer[0]));
    }
}